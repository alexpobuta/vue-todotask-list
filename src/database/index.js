import VuexORM from "@vuex-orm/core";
import Tasks from "@/models/Tasks";

// Create a new instance of Database.
const database = new VuexORM.Database()

// Register Models to Database.
database.register(Tasks)

export default database

