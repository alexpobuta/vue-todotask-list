import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import VueTailwind from 'vue-tailwind'
import '@/assets/css/tailwind.css'

Vue.config.productionTip = false


new Vue({
  render: h => h(App)
}).$mount('#app')

Vue.use(Vuex);
Vue.use(VueTailwind);

